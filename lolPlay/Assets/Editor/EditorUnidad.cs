﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System;

public enum TIPOEDITOR { Habilidades, Prefab}

public class EditorUnidad : EditorWindow {
    
    Vector2 scrollPositionBarra;
    Vector2 scrollPositionContenido;
    
    GameManager manager;
    BaseDatos baseDatos;

    int idUnidad = 1;
    int idSpell = 0;

    TIPOEDITOR tipo = TIPOEDITOR.Habilidades;
    string[] nombreDir = new string[5] { "↑", "┐", "→", " ┘", "↓" };
    string[] nombreDirInv = new string[5] { "↑", "┌", "←", "└", "↓" };
    bool invertido = false;
    Personaje personaje = null;
    int tam = 80;
    float tiempoActual = 0, tiempoNec;
    int spriteActual = 0, direccionActual;

    bool cambiarPosicion = false;
    int posicionInicial = 0;
    int posicionFinal = 0;

    float minValue = 0;
    float maxValue = 0;
    int decenas = 0;

    const float deltaTime = 0.15f;

    [MenuItem("GameEditor/Editor de unidades", false, 140)]
    static void Init() {
        EditorUnidad window = (EditorUnidad)EditorWindow.GetWindow(typeof(EditorUnidad));
        
        window.position = new Rect(0, 20, 800, 400);

        window.Show();
    }

    [MenuItem("GameEditor/Crear nuevo archivo", false, 150)]
    public static void CreateAsset() {
        BaseDatos asset = ScriptableObject.CreateInstance<BaseDatos>();
        AssetDatabase.CreateAsset(asset, "Assets/NuevoDocumentoDeCartas.asset");
        AssetDatabase.SaveAssets();
        EditorUtility.FocusProjectWindow();
        Selection.activeObject = asset;
    }

    void Awake() {
        manager = (GameManager)GameObject.FindObjectOfType(typeof(GameManager));
        minSize = new Vector2(800, 400);
    }

    void OnEnable() {
        //Debug.Log ("Editor desbloqueado");
    }

    void OnInspectorUpdate() {
        if(tipo==TIPOEDITOR.Habilidades || personaje==null || personaje.sprites.movimiento[0].animacion.Length == 0 || personaje.sprites.movimiento[0].animacion[0] == null) 
            return;
        
        tiempoActual += deltaTime;
        if (tiempoActual>tiempoNec) {
            spriteActual++;
            if (spriteActual>=personaje.sprites.movimiento[direccionActual].animacion.Length)
                spriteActual = 0;

            tiempoActual = 0;
            tiempoNec = personaje.sprites.velocidad;
        }

        Repaint();
    }

    void OnDestroy() {
        if(manager == null)
            return;
        if(EditorUtility.DisplayDialog("¿Quieres guardar la información de las cartas?", "Si no lo guardas se perderá la información de los datos.", "Si", "No")) {
            if(manager != null && baseDatos != null) {
                EditorUtility.SetDirty(baseDatos);
            } else {
                Debug.LogError("No se ha podido guardar");
            }
        }
    }

    /*void OnInspectorUpdate() {
        if(EditorApplication.isPlaying)
            Repaint();
    }*/
    
    void OnGUI() {
        if(manager == null) {
            EditorGUILayout.HelpBox("No hay manager en esta escena", MessageType.Warning);
            PedirManager();
            return;
        } else {
            if (manager.desactivarEditor) {
                EditorGUILayout.HelpBox("Editor bloqueado manualmente por el gameManager", MessageType.Info);
                if (GUILayout.Button ("Activar editor")) {
                    manager.desactivarEditor = false;
                }
                return;
            }
        }
        
        if(manager != null && baseDatos == null) {
            baseDatos = manager.baseDatos;
            if(baseDatos == null) {
                EditorGUILayout.HelpBox("El GameManager no tiene ligado la base de datos.\n" +
                                         "Por favor, ligalo abajo para poder manipularlo.\n" + 
                                         "(Se recomienda ligarlo directamente al manager).", MessageType.Error);

                baseDatos = (BaseDatos)EditorGUILayout.ObjectField("Archivo de las cartas: ", baseDatos, typeof(BaseDatos), false, GUILayout.MinWidth(50), GUILayout.MaxWidth(300));
               
                return;
            }
        }

        if (baseDatos.campeones == null || baseDatos.campeones.Length == 0) {
            baseDatos.campeones = new Campeon[1];
        }

        MostrarGUI();
    }

    void PedirManager() {
        if(manager != null) {
            return;
        } else {
            manager = (GameManager)GameObject.FindObjectOfType(typeof(GameManager));
            if(manager == null) {
                GUILayout.Label("Hace falta un script \"GameManager\" para hacer funcionar este plugin.", EditorStyles.boldLabel/*, EditorStyles.largeLabel*/);
                GUILayout.Label("No se ha encontrdo el Script, insertalo aquí:", EditorStyles.boldLabel);
                manager = (GameManager)EditorGUILayout.ObjectField("GameManager: ", manager, typeof(GameManager), true, GUILayout.MinWidth(50), GUILayout.MaxWidth(300));
                GUILayout.Label("Si no tienes ninguno puedes crear uno aqui:", EditorStyles.boldLabel);
                if(GUILayout.Button("Crear nuevo", GUILayout.MinWidth(20), GUILayout.MaxWidth(100))) {
                    GameObject _obj = new GameObject();
                    _obj.name = "GameManager";
                    _obj.AddComponent<GameManager>();
                }
            }
        }
    }

    void MostrarGUI() {
        if(idUnidad >= baseDatos.campeones.Length)
            idUnidad = baseDatos.campeones.Length-1;

        GUILayout.BeginHorizontal();

        SelectorUnidad();

        GUILayout.BeginVertical();
        
        EdicionUnidad();
        
        GUILayout.EndVertical();
        GUILayout.EndHorizontal();
    }

    void SelectorUnidad() {
        GUILayout.BeginVertical();
        GUILayout.BeginVertical("box", GUILayout.Width(230));
        GUILayout.Label("Lista de cartas", EditorStyles.centeredGreyMiniLabel);
        scrollPositionBarra = GUILayout.BeginScrollView(scrollPositionBarra, GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true));

        for(int i = 0; i < baseDatos.campeones.Length; i++) {
            string _nombre = "Campeon #" + (i + 1);
            if(i != baseDatos.campeones.Length)
                if(baseDatos.campeones[i].nombreCampeon != "")
                    _nombre = baseDatos.campeones[i].nombreCampeon + "(#" + (i + 1) + ")";
            if(idUnidad == i) {
                GUILayout.Label(_nombre, GUILayout.Width(200));
            } else {
                if(GUILayout.Button(_nombre, GUILayout.Width(200))) {
                    GUIUtility.keyboardControl = 0;
                    idUnidad = i;
                    minValue = 0;
                    maxValue = 0;
                    if(cambiarPosicion) {
                        posicionFinal = idUnidad;
                        MoverUnidad(posicionInicial, posicionFinal);
                        cambiarPosicion = false;
                        posicionInicial = 0;
                        posicionFinal = 0;
                    }
                }
            }
        }
        if(GUILayout.Button("+", GUILayout.Width(200))) {
            GUIUtility.keyboardControl = 0;
            CrearNuevaCarta();
            idUnidad = baseDatos.campeones.Length - 1;
        }
        GUILayout.EndScrollView();
        GUILayout.EndVertical();
        GUILayout.EndVertical();
    }

    void EdicionUnidad() {
        GUILayout.BeginHorizontal();
        if(!cambiarPosicion) {
            if(GUILayout.Button("Cambiar posición carta.", GUILayout.MinWidth(60), GUILayout.MaxWidth(300))) {
                posicionInicial = idUnidad;
                cambiarPosicion = true;
            }
            tipo = (TIPOEDITOR) EditorGUILayout.EnumPopup(tipo, GUILayout.MinWidth(60), GUILayout.MaxWidth(300));
        } else {
            if(GUILayout.Button("Cancelar cambio de posición.", GUILayout.MinWidth(60), GUILayout.MaxWidth(300))) {
                posicionInicial = 0;
                cambiarPosicion = false;
            }
            EditorGUILayout.HelpBox("Selecciona donde quieres cambiar de posición esta unidad usando el menú de la izquierda", MessageType.Info);
        }
        GUILayout.EndHorizontal();
        
        if (tipo == TIPOEDITOR.Habilidades) {
            EditarHabilidades();
        }else if (tipo == TIPOEDITOR.Prefab) {
            EditarPrefab();
        }
        
    }

    void EditarHabilidades () {
        GUILayout.BeginVertical("box");
        GUILayout.Label("Información de la unidad.", EditorStyles.boldLabel);

        baseDatos.campeones[idUnidad].spriteCampeon = (Sprite)EditorGUILayout.ObjectField("Sprite: ", baseDatos.campeones[idUnidad].spriteCampeon, typeof(Sprite), false);
        baseDatos.campeones[idUnidad].prefabCampeon = (GameObject)EditorGUILayout.ObjectField("Prefab: ", baseDatos.campeones[idUnidad].prefabCampeon, typeof(GameObject), false);
        baseDatos.campeones[idUnidad].nombreCampeon = EditorGUILayout.TextField("Nombre unidad", baseDatos.campeones[idUnidad].nombreCampeon);

        GUILayout.Space(10);

        baseDatos.campeones[idUnidad].costeMana = EditorGUILayout.FloatField("Coste mana: ", baseDatos.campeones[idUnidad].costeMana);

        baseDatos.campeones[idUnidad].vida = EditorGUILayout.FloatField("Vida: ", baseDatos.campeones[idUnidad].vida);
        baseDatos.campeones[idUnidad].dañoAtaque = EditorGUILayout.FloatField("Daño ataque: ", baseDatos.campeones[idUnidad].dañoAtaque);
        baseDatos.campeones[idUnidad].velAtaque = EditorGUILayout.FloatField("Velocidad ataque: ", baseDatos.campeones[idUnidad].velAtaque);
        baseDatos.campeones[idUnidad].rangoAtaque = EditorGUILayout.FloatField("Rango ataque: ", baseDatos.campeones[idUnidad].rangoAtaque);
        baseDatos.campeones[idUnidad].velMovimiento = EditorGUILayout.FloatField("Movimiento: ", baseDatos.campeones[idUnidad].velMovimiento);

        EditorGUILayout.HelpBox("Si uno de esos valores aumenta por nivel pon -1 ó -2", MessageType.Info);

        GUILayout.EndVertical();

        GUILayout.Space(10);

        GUILayout.BeginVertical("box");
        GUILayout.Label("Información de las habilidades (Del nivel 1 al 18).", EditorStyles.boldLabel);
        scrollPositionContenido = GUILayout.BeginScrollView(scrollPositionContenido, GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true));

        if(baseDatos.campeones[idUnidad].habilidades == null) {
            baseDatos.campeones[idUnidad].habilidades = new Habilidad[5];
        }

        GUILayout.BeginHorizontal();
        for(int i = 0; i < baseDatos.campeones[idUnidad].habilidades.Length; i++) {
            if(baseDatos.campeones[idUnidad].habilidades[i] == null) {
                baseDatos.campeones[idUnidad].habilidades[i] = new Habilidad();
            }
            if(baseDatos.campeones[idUnidad].habilidades[i].spriteHabilidad == null) {
                if(GUILayout.Button(baseDatos.campeones[idUnidad].habilidades[i].nombreHabilidad, GUILayout.MaxWidth(100), GUILayout.Height(80))) {
                    GUIUtility.keyboardControl = 0;
                    idSpell = i;
                }
            } else {
                if(GUILayout.Button(baseDatos.campeones[idUnidad].habilidades[i].spriteHabilidad.texture, GUILayout.MaxWidth(100), GUILayout.Height(80))) {
                    GUIUtility.keyboardControl = 0;
                    idSpell = i;
                }
            }
        }
        if(GUILayout.Button("+", GUILayout.MaxWidth(100), GUILayout.Height(80))) {
            CrearHabilidad();
            GUIUtility.keyboardControl = 0;
            idSpell = baseDatos.campeones[idUnidad].habilidades.Length - 1;
        }
        GUILayout.EndHorizontal();

        if(baseDatos.campeones[idUnidad].habilidades.Length > 0) {
            if(idSpell >= baseDatos.campeones[idUnidad].habilidades.Length)
                idSpell = 0;

            GUILayout.BeginVertical("box");
            baseDatos.campeones[idUnidad].habilidades[idSpell].spriteHabilidad = (Sprite)EditorGUILayout.ObjectField("Sprite: ", baseDatos.campeones[idUnidad].habilidades[idSpell].spriteHabilidad, typeof(Sprite), false);
            baseDatos.campeones[idUnidad].habilidades[idSpell].nombreHabilidad = EditorGUILayout.TextField("Nombre estadística", baseDatos.campeones[idUnidad].habilidades[idSpell].nombreHabilidad);
            GUILayout.Space(10);
            if(baseDatos.campeones[idUnidad].habilidades[idSpell].valores.Length != 18) {
                baseDatos.campeones[idUnidad].habilidades[idSpell].valores = new float[18];
            }
            for(int y = 0; y < 3; y++) {
                EditorGUILayout.BeginHorizontal();
                for(int x = 0; x < 6; x++) {
                    baseDatos.campeones[idUnidad].habilidades[idSpell].valores[x + y * 6] = EditorGUILayout.FloatField(/*"nvl"+(x+(y*6)+1), */baseDatos.campeones[idUnidad].habilidades[idSpell].valores[x + y * 6], GUILayout.ExpandWidth(true));
                }
                EditorGUILayout.EndHorizontal();
            }
            GUILayout.Space(5);

            EditorGUILayout.BeginVertical("box");
            EditorGUILayout.BeginHorizontal();
            minValue = EditorGUILayout.FloatField("Valor nvl1: ", minValue);
            maxValue = EditorGUILayout.FloatField("Valor nvl18: ", maxValue);
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.BeginHorizontal();
            decenas = EditorGUILayout.IntField("Unidades a redondear: ", decenas);
            if(GUILayout.Button("Convertir")) {
                Convertir(idSpell, decenas);
            }
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.EndVertical();
            GUILayout.EndVertical();
        }

        GUILayout.EndScrollView();
        GUILayout.EndVertical();
    }

    void EditarPrefab () {
        GUILayout.BeginVertical("box");
        GUILayout.Label("Información de la unidad.", EditorStyles.boldLabel);

        baseDatos.campeones[idUnidad].spriteCampeon = (Sprite)EditorGUILayout.ObjectField("Sprite: ", baseDatos.campeones[idUnidad].spriteCampeon, typeof(Sprite), false);
        baseDatos.campeones[idUnidad].prefabCampeon = (GameObject)EditorGUILayout.ObjectField("Prefab: ", baseDatos.campeones[idUnidad].prefabCampeon, typeof(GameObject), false);
        baseDatos.campeones[idUnidad].nombreCampeon = EditorGUILayout.TextField("Nombre unidad", baseDatos.campeones[idUnidad].nombreCampeon);
        GUILayout.EndVertical();

        GUILayout.BeginVertical("box");
        scrollPositionContenido = GUILayout.BeginScrollView(scrollPositionContenido, GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true));
        GUILayout.Label("Información del prefab.", EditorStyles.boldLabel);
        
        if (baseDatos.campeones[idUnidad].prefabCampeon==null) {
            EditorGUILayout.HelpBox("Este campeón no tiene gameObject válido", MessageType.Warning);
            GUILayout.EndScrollView();
            GUILayout.EndVertical();
            return;
        }

        personaje = baseDatos.campeones[idUnidad].prefabCampeon.GetComponent<Personaje>();
        if (personaje == null) {
            EditorGUILayout.HelpBox("Este campeón no tiene un script 'Personaje' válido", MessageType.Warning);
            GUILayout.EndScrollView();
            GUILayout.EndVertical();
            return;
        }

        if (personaje.usarModelo) {
            EditorGUILayout.HelpBox("Este campeón está haciendo uso del modelo en 3D", MessageType.Warning);
            if (GUILayout.Button ("Cambiar a modelo de prefabs")) {
                personaje.usarModelo = false;
            }
            GUILayout.EndScrollView();
            GUILayout.EndVertical();
            return;
        }

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.BeginVertical();
        if(direccionActual >= personaje.sprites.movimiento.Length)
            direccionActual = 0;

        for (int i = 0; i < personaje.sprites.movimiento.Length; i++) {
            EditorGUILayout.BeginHorizontal();

            EditorGUILayout.BeginVertical();
            if (GUILayout.Button ((i<=5) ? nombreDir[i] : "Otro", GUILayout.Width (tam), GUILayout.Height ((i > 0 && i < 4) ? tam/2 : tam))) {
                direccionActual = i;
                invertido = false;
            }
            if (i>0&&i<4) {
                if(GUILayout.Button((i <= 5) ? nombreDirInv[i] : "Otro", GUILayout.Width(tam), GUILayout.Height(tam/2))) {
                    direccionActual = i;
                    invertido = true;
                }
            }
            EditorGUILayout.EndVertical();

            for (int o = 0; o < personaje.sprites.movimiento[i].animacion.Length; o++) {
                personaje.sprites.movimiento[i].animacion[o] = (Sprite)EditorGUILayout.ObjectField(personaje.sprites.movimiento[i].animacion[o], typeof(Sprite), false, GUILayout.Width(tam), GUILayout.Height(tam));
            }
            EditorGUILayout.EndHorizontal();
        }
        EditorGUILayout.EndVertical();

        Rect _rect = EditorGUILayout.BeginVertical("box", GUILayout.MinHeight(tam), GUILayout.MaxHeight (tam*3), GUILayout.MinWidth(tam), GUILayout.MaxWidth(tam * 3));
        
        GUILayout.Label(tiempoActual.ToString());

        if(personaje.sprites.movimiento[0].animacion.Length > 0 && personaje.sprites.movimiento[0].animacion[0] != null) {
            DrawSprite(_rect, personaje.sprites.movimiento[direccionActual].animacion[spriteActual], invertido);
        }

        EditorGUILayout.EndVertical();
        EditorGUILayout.EndHorizontal();

        GUILayout.EndScrollView();
        GUILayout.EndVertical();
    }

    void Convertir (int index, int decenas) {
        float diferencia = maxValue - minValue;
        float unidad = diferencia / (baseDatos.campeones[idUnidad].habilidades[index].valores.Length-1);
        for (int i = 0; i < baseDatos.campeones[idUnidad].habilidades[index].valores.Length; i++) {
            baseDatos.campeones[idUnidad].habilidades[index].valores[i] = Mathf.Round((minValue + (unidad * i))*Mathf.Pow (10, decenas))/ Mathf.Pow(10, decenas);
        }
        minValue = 0;
        maxValue = 0;
        GUIUtility.keyboardControl = 0;
    }

    void MoverUnidad(int posInicial, int posFinal) {
        Campeon _antiguaCarta = baseDatos.campeones[posInicial];
        baseDatos.campeones[posInicial] = baseDatos.campeones[posFinal];
        baseDatos.campeones[posFinal] = _antiguaCarta;
    }
    
    void CrearNuevaCarta() {
        Campeon[] _copiaCartas = baseDatos.campeones;
        baseDatos.campeones = new Campeon[baseDatos.campeones.Length + 1];

        for(int i = 0; i < _copiaCartas.Length; i++) {
            baseDatos.campeones[i] = _copiaCartas[i];
        }
        baseDatos.campeones[baseDatos.campeones.Length - 1] = new Campeon();
        baseDatos.campeones[baseDatos.campeones.Length - 1].nombreCampeon = "";
    }

    void BorrarUltimaCarta() {
        Campeon[] _copiaCartas = baseDatos.campeones;
        baseDatos.campeones = new Campeon[baseDatos.campeones.Length - 1];

        for(int i = 0; i < _copiaCartas.Length; i++) {
            baseDatos.campeones[i] = _copiaCartas[i];
        }
    }

    void CrearHabilidad () {
        if (baseDatos.campeones[idUnidad].habilidades.Length >= 5) {
            Debug.LogError("Número máximo de habilidades permitidas, aumenta manualmente este límite");
            return;
        }
        Habilidad[] _copiaHabilidad = baseDatos.campeones[idUnidad].habilidades;
        baseDatos.campeones[idUnidad].habilidades = new Habilidad[baseDatos.campeones[idUnidad].habilidades.Length + 1];

        for(int i = 0; i < _copiaHabilidad.Length; i++) {
            baseDatos.campeones[idUnidad].habilidades[i] = _copiaHabilidad[i];
        }
        baseDatos.campeones[idUnidad].habilidades[baseDatos.campeones[idUnidad].habilidades.Length - 1] = new Habilidad();
        baseDatos.campeones[idUnidad].habilidades[baseDatos.campeones[idUnidad].habilidades.Length - 1].nombreHabilidad = "";
    }

    void DrawSprite (Rect rect, Sprite sprite, bool _invertido) {
        if(sprite == null)
            return;

        Rect _rect2 = new Rect(sprite.rect);
        float tamX = sprite.texture.width;
        float tamY = sprite.texture.height;
        _rect2 = new Rect(_rect2.x / tamX, _rect2.y / tamY, _rect2.width / tamX, _rect2.height / tamY);

        rect.size = new Vector2((!_invertido) ? sprite.rect.width : -sprite.rect.width, sprite.rect.height);
        rect.position = new Vector2(rect.position.x+sprite.rect.width/2, rect.position.y+sprite.rect.height/2);

        Graphics.DrawTexture(rect, sprite.texture, _rect2, 1, 1, 1, 1);
    }
}