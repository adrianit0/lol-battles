﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class ExtraerAnim : MonoBehaviour {

    public AnimationClip anim;
    public string nombre;
    

    public void GuardarPlanoAsset() {
        AnimationClip newAnim = new AnimationClip();
        newAnim = anim;
        AssetDatabase.CreateAsset(newAnim, string.Format("Assets/{0}.asset", nombre)); // saves to "assets/"
    }
}