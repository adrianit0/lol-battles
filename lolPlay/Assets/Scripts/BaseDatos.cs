﻿using UnityEngine;

public class BaseDatos : ScriptableObject {
    public Campeon[] campeones;
}

[System.Serializable]
public class Campeon {
    public string nombreCampeon;
    public Sprite spriteCampeon;
    public GameObject prefabCampeon;

    public float costeMana = 50;
    public float vida, dañoAtaque, velAtaque, rangoAtaque, velMovimiento;

    public float tiempoDisparo = 0.1f;

    public Habilidad[] habilidades;
}

[System.Serializable]
public class Habilidad {
    public string nombreHabilidad;
    public Sprite spriteHabilidad;

    public float[] valores = new float[18];
}