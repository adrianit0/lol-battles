﻿using UnityEngine;
using System.Collections;

public class Misil : MonoBehaviour {

    public GameManager manager;

    public DañoRecibido daño;
    public ObjectLoL origenObj;
    public ObjectLoL objetivoObj;
    public Vector3 objetivo;

    public BANDO bando;
    public LayerMask layerPersonaje;

    public float velocidad;
    float realVel;

    public AudioClip sonidoImpacto;
    public GameObject sistemaParticulaFinal;

    const float separacionMaxima = 0.05f;

    ParticleSystem particle;
    Rigidbody rb;
    Collider coll;

    void Awake () {
        particle = GetComponent<ParticleSystem>();
        rb = GetComponent<Rigidbody>();
        coll = GetComponent<Collider>();
        coll.isTrigger = true;

        

        realVel = velocidad / 600 * Time.fixedDeltaTime;
    }

    void Start () {
        if(particle != null) {
            particle.startRotation = Quaternion.LookRotation(transform.position + ((objetivoObj != null) ? objetivoObj.transform.position : objetivo)).y + 180;
            particle.Stop();
            particle.Play();
        }
    }

    void FixedUpdate() {
        Vector3 dif = - transform.position + ((objetivoObj!=null) ? objetivoObj.transform.position : objetivo);
        Vector3 movimiento = dif.normalized * realVel;

        /*Quaternion nuevaRotacion = Quaternion.LookRotation(movimiento);
        rb.MoveRotation(nuevaRotacion);*/

        rb.MovePosition(transform.position + movimiento);
        
        if (separacionMaxima > Vector3.Distance (transform.position, ((objetivoObj != null) ? objetivoObj.transform.position : objetivo))) {
            if (objetivoObj!=null) {
                ObjectLoL _personaje = objetivoObj.GetComponent<ObjectLoL>();
                Impactar(_personaje);
            } else {
                RaycastHit[] ray = Physics.SphereCastAll(transform.position, separacionMaxima, Vector3.up, layerPersonaje);

                ObjectLoL lastObject = null;

                for(int i = 0; i < ray.Length; i++) {
                    ObjectLoL personaje = ray[i].transform.GetComponent<ObjectLoL>();

                    if(personaje != null && personaje.bando != bando) {
                        if(lastObject == null || Vector3.Distance(lastObject.transform.position, transform.position) > Vector3.Distance(personaje.transform.position, transform.position)) {
                            lastObject = personaje;
                        }
                    }
                }

                if (lastObject!=null) {
                    Impactar(lastObject);
                } 
            }

            Destroy(this.gameObject);
        }
    }

    void Impactar (ObjectLoL lastObject) {
        origenObj.interfazObjeto.PreAtaque(ref daño);
        lastObject.InflingirDaño(daño);
        manager.soundManager.Play(sonidoImpacto, 0.1f);
    }
}
