﻿
using System.Collections.Generic;

public class DañoRecibido {

    public float dañoAtaque;

    public List<Buff> bufos = new List<Buff>();

    public DañoRecibido (float _daño, params Buff[] _bufos) {
        dañoAtaque = _daño;

        for (int i = 0; i < _bufos.Length; i++) {
            bufos.Add(_bufos[i]);
        }
    }
}
