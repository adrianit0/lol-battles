﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Contenido : MonoBehaviour {

    GameObject contenido;

    public GameObject rangoPersonaje;

    public RectTransform canvasPrincipal;
    public Image fondoVida;
    public Image imagenVida;
    public Text textoNivel;

    public Sprite[] colorSprite;
    public Color[] color;

    void Awake () {
        contenido = gameObject;
    }

    public void SetCanvas (float rango, int nivel, BANDO bando) {
        rangoPersonaje.transform.localScale = new Vector3(rango, rangoPersonaje.transform.lossyScale.y, rango);
        imagenVida.fillAmount = 1;
        if (textoNivel!=null)
            textoNivel.text = nivel.ToString();
        imagenVida.sprite = colorSprite[(bando == BANDO.Aliado) ? 0 : 1];
        fondoVida.color = color[(bando == BANDO.Aliado) ? 0 : 1];
    }

    public void CambiarVida (float vidaActual, float vidaTotal) {
        if (imagenVida!= null)
            imagenVida.fillAmount = vidaActual / vidaTotal;
        if(vidaActual <= 0)
            canvasPrincipal.gameObject.SetActive(false);
    }

    public void AjustarCamara (Vector3 eulerRotation) {
        contenido.transform.localRotation = Quaternion.Euler(new Vector3(contenido.transform.localRotation.eulerAngles.x, eulerRotation.y * -1, contenido.transform.localRotation.eulerAngles.z));
    }
}
