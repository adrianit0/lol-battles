﻿using UnityEngine;
using System.Collections;

public class Estructura : ObjectLoL, IObjectLoL {

    public Contenido contenido;

    float realRange = 0.5f;
    
	public void Configurar () {
        if(configurado) {
            Debug.Log("El objeto ya ha sido configurado con anterioridad.");
            return;
        }
        configurado = true;

        realRange = rangoAtaque / 400;

        InvokeRepeating("ControlMove", 0, deltaMove);

        interfazObjeto = (IObjectLoL)this;

        contenido.SetCanvas(realRange * 2, nivel, bando);
        contenido.AjustarCamara(transform.rotation.eulerAngles);
    }

    public void OnDieObject () {
        manager.listaTorres.Remove(this);
        Destruir();
    }

    public void PreAtaque (ref DañoRecibido daño) {
        //ACTUALMENTE NO PASA NADA
    }

    public void RecibirDaño (DañoRecibido dañoRecibido) {
        vidaActual -= dañoRecibido.dañoAtaque;
        contenido.CambiarVida(vidaActual, vidaMaxima);
        
        if(vidaActual <= 0) {
            vidaActual = 0;
            OnDieObject();
        }
    }

    void ControlMove () {
        if(objetivo == null) {
            EncontrarObjetivo();
        }
    }

    void FixedUpdate() {
        if(objetivo != null && (realRange > Vector3.Distance(transform.position, objetivo.transform.position))) {
            if(lastAttack > (1 / velAtaque)) {
                Atacar();
                lastAttack = 0;
            }
        }
        lastAttack += Time.fixedDeltaTime;
    }
}
