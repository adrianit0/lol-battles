﻿using UnityEngine;
using System.Collections;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class EstadoJuego {

	public DatosAGuardar datos;

	private string rutaArchivo;
	
    public EstadoJuego() {
        rutaArchivo = GetDataPath();
        Cargar();
    }

    public string GetDataPath() {
        return Application.persistentDataPath + "/partida.dat";
    }

    public void Guardar (DatosAGuardar info) {
        datos = info;
        Guardar();
    }

    public DatosAGuardar extraerDatos () {
        return datos;
    }

    public void BorrarDatos () {
        File.Delete(rutaArchivo);
    }
    
	void Guardar(){
		BinaryFormatter bf = new BinaryFormatter();
		FileStream file = File.Create(rutaArchivo);
		DatosAGuardar _datos = new DatosAGuardar ();
		
		_datos = datos;
		
		bf.Serialize(file, _datos);
		file.Close();
	}
	
	void Cargar(){
		if(File.Exists(rutaArchivo)){
			BinaryFormatter bf = new BinaryFormatter();
			FileStream file = File.Open (rutaArchivo, FileMode.Open);
			DatosAGuardar _datos = (DatosAGuardar)bf.Deserialize(file);
			
			datos = _datos;
			
			file.Close ();
		}else{
			BorrarPartida();
		}
	}
	
	public void BorrarPartida(){
		datos = new DatosAGuardar();
		Guardar();
	}
}

[Serializable]
public class DatosAGuardar{
    public UnidadInfo[] unidad;
    public Mazo mazo = new Mazo();

	//Constructor
	public DatosAGuardar () {
		unidad = new UnidadInfo[0];
	}
}

[Serializable]
public class UnidadInfo {
    public int[] nivel;

    public UnidadInfo() {
        nivel = new int[18];
    }
}

[Serializable]
public class Mazo {
    public Carta[] cartas;

    public Mazo() {
        cartas = new Carta[8];
    }
}

[Serializable]
public class Carta {
    public int cartaID;
    public int nivel;

    public Carta (int _carta, int _nivel) {
        cartaID = _carta;
        nivel = Mathf.Clamp(_nivel, 1, 18);
    }
}