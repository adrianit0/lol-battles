﻿using UnityEngine;
using System.Collections;

public interface IPersonaje {

    void OnAttack (ref DañoRecibido damage);
    void OnMove();
    void OnDie();
    void OnDamageReceive(DañoRecibido damage);
    void OnUpdate();
}
