﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum RangoUnidad {Ranged, Meele}

public class Personaje : ObjectLoL, IObjectLoL {
    
    public RangoUnidad rango = RangoUnidad.Meele;
    public LADOSALIDA lado = LADOSALIDA.Derecha;

    public Contenido contenido;

    public List<Buff> buffos = new List<Buff>();
    //KOCKUP AJUSTES
    bool stuneado = false;
    Buff buffoKnockUp;

    public bool enMovimiento = true;

    public bool usarModelo = true;
    public SpriteRenderer render;
    public SpriteRenderer renderSombra;
    public SpriteAnim sprites = new SpriteAnim();
    float tiempoSprite;
    int spriteActual = 0, giroActual = 0;

    const float sep = 0.10f;
    
    Vector3 proximaPosicion;
    Vector3 movimiento;
    float realRange, realVelMov = 0, buffVelMov = 0, aumentoDaño;
    

    public IPersonaje interfazPersonaje;
    
    public void Configurar() {
        if (configurado) {
            Debug.Log("El objeto ya ha sido configurado con anterioridad.");
            return;
        }
        configurado = true;

        InvokeRepeating("ControlMove", deltaMove, deltaMove);

        interfazObjeto = (IObjectLoL) this;

        realRange = rangoAtaque / difRango;
        realVelMov = velMovimiento / 600 * Time.fixedDeltaTime;

        contenido.SetCanvas(realRange * 2, nivel, bando);

        if (manager!=null)
            proximaPosicion = manager.siguienteEntidad(transform.position, bando, lado);
    }

    public void OnDieObject () {
        interfazPersonaje.OnDie();
        
        manager.listaPersonajes.Remove(this);
        Destruir();
    }

    void FixedUpdate() {
        if(stuneado) {
            if(buffoKnockUp != null)
                MoverKnockUp();
            return;
        }

        if (!usarModelo&&render!=null) {
            tiempoDisparo += Time.fixedDeltaTime;
            if (tiempoDisparo>sprites.velocidad) {
                tiempoDisparo = 0;
                spriteActual = (spriteActual+1 >= sprites.movimiento[giroActual].animacion.Length) ? 0 : spriteActual + 1;
                render.sprite = sprites.movimiento[giroActual].animacion[spriteActual];
                renderSombra.sprite = sprites.movimiento[giroActual].animacion[spriteActual];
            }
        }

        if (objetivo==null) {
            Mover(proximaPosicion - transform.position);
        } else if (objetivo!=null) {
            if (realRange > Vector3.Distance (transform.position, objetivo.transform.position)) {
                if (enMovimiento) {
                    animator.SetBool("Movimiento", false);
                    enMovimiento = false;
                }
                
                if (lastAttack > (1/velAtaque)) {
                    Girar(objetivo.transform.position - transform.position);
                    Atacar(tiempoDisparo);
                    animator.SetTrigger("Atacar");
                    lastAttack = 0;
                }
            } else {
                Mover(objetivo.transform.position - transform.position);
            }
        }

        lastAttack += Time.fixedDeltaTime;
    }

    void LateUpdate () {
        if (/*giro != transform.rotation.eulerAngles && */contenido != null) {
            contenido.AjustarCamara(transform.rotation.eulerAngles);
        }
    }

    void ControlMove () {
        if(manager != null && sep > Vector3.Distance(transform.position, proximaPosicion)) {
            proximaPosicion = manager.siguienteEntidad(transform.position, bando, lado);
        }

        if (objetivo == null) {
            EncontrarObjetivo();
        }

        ControlBuff();

        interfazPersonaje.OnUpdate();
    }

    public void PreAtaque (ref DañoRecibido daño) {
        interfazPersonaje.OnAttack(ref daño);

        //NADA MAS
    }
    
    void Mover(float h, float v) {
        movimiento.Set(h, 0, v);
        movimiento = movimiento.normalized * (realVelMov + buffVelMov);

        animator.SetBool("Movimiento", true);
        enMovimiento = true;

        Girar();
        rb.MovePosition(transform.position + movimiento);
    }

    void Mover (Vector3 diferencia) {
        Mover(diferencia.x, diferencia.z);
    }

    void MoverKnockUp () {
        coll.isTrigger = true;
        buffoKnockUp.tiempoActual += Time.fixedDeltaTime;
        transform.position = new Vector3(transform.position.x, GetHeight(), transform.position.z);
        rb.MovePosition(transform.position-(buffoKnockUp.direccion.normalized*buffoKnockUp.distancia*Time.fixedDeltaTime));

        if (buffoKnockUp.tiempoActual>=buffoKnockUp.tiempoTotal) {
            EliminarBuffo(buffoKnockUp);
            buffoKnockUp = null;
            transform.position = new Vector3(transform.position.x, 0, transform.position.z);
            coll.isTrigger = false;
        }
    }

    float GetHeight () {
        if(buffoKnockUp == null || manager == null)
            return 0;
        return (manager.curva.Evaluate(buffoKnockUp.tiempoActual / buffoKnockUp.tiempoTotal)*buffoKnockUp.altura);
    }

    void Girar() {
        Girar(movimiento);
    }

    public void Girar (Vector3 rotacion) {
        if(!usarModelo) {
            rotacion.Set(rotacion.x, 0, rotacion.z);
            float _rotacion = Quaternion.LookRotation(rotacion).eulerAngles.y;
            if(_rotacion < 22.5f)
                giroActual = 0;
            else
                giroActual = 1;
            return;
        }
            
        if(movimiento != Vector3.zero) {
            rotacion.Set(rotacion.x, 0, rotacion.z);
            Quaternion nuevaRotacion = Quaternion.LookRotation(rotacion);
            rb.MoveRotation(nuevaRotacion);

            if(contenido != null)
                contenido.AjustarCamara(transform.rotation.eulerAngles);
        }
    }

    public void RecibirDaño (DañoRecibido dañoRecibido) {
        vidaActual -= Mathf.Clamp(dañoRecibido.dañoAtaque*(1+Mathf.Clamp(aumentoDaño, -10, 10)), 0, vidaMaxima);
        contenido.CambiarVida(vidaActual, vidaMaxima);

        if(dañoRecibido.bufos.Count > 0)
            AplicarBuffo(dañoRecibido.bufos);
        
        interfazPersonaje.OnDamageReceive(dañoRecibido);

        if (vidaActual<=0) {
            vidaActual = 0;
            OnDieObject();
        }
        //Resto de tonterias
    }

    public void CurarVida (float cantidad) {
        vidaActual = Mathf.Clamp(Mathf.Round(vidaActual+cantidad), 0, vidaMaxima);

        contenido.CambiarVida(vidaActual, vidaMaxima);
    }

    void ControlBuff() {
        for(int i = 0; i < buffos.Count; i++) {
            switch(buffos[i].tipo) {
                case TIPOBUFF.DañoSegundo:
                    InflingirDaño(new DañoRecibido((buffos[i].cantidadTotal)? (buffos[i].cantidad * deltaMove)/buffos[i].tiempoTotal : buffos[i].cantidad * deltaMove));
                    break;
                case TIPOBUFF.CuraSegundo:
                    CurarVida((buffos[i].cantidadTotal) ? (buffos[i].cantidad * deltaMove) / buffos[i].tiempoTotal : buffos[i].cantidad * deltaMove);
                    break;
                case TIPOBUFF.Acelerar:
                break;
                //NADA
                case TIPOBUFF.KnockUp:

                break;
            }

            if (buffos[i].tipoTemporal == TIEMPO.Temporal) {
                if (buffos[i].tipo!=TIPOBUFF.KnockUp)
                    buffos[i].tiempoActual += deltaMove;
                if(buffos[i].tiempoActual >= buffos[i].tiempoTotal) {
                    EliminarBuffo(buffos[i]);
                }
            }
        }
    }

    public void AplicarBuffo (Buff bufo) {
        if (bufo.aditivo == ADITIVO.unico || bufo.aditivo == ADITIVO.renovar) {
            for (int i = 0; i < buffos.Count; i++) {
                if (buffos[i].nombreID == bufo.nombreID) {
                    //Debug.Log("Buff "+bufo.nombreID+" repetido");
                    if(bufo.aditivo == ADITIVO.renovar)
                        buffos[i].RenovarEfecto();

                    return;
                }
            }
        }

        switch (bufo.tipo) {
            case TIPOBUFF.DañoSegundo:
            case TIPOBUFF.CuraSegundo:
                //NADA
            break;
            case TIPOBUFF.Acelerar:
                buffVelMov += realVelMov * bufo.cantidad;
            break;
            case TIPOBUFF.KnockUp:
                buffoKnockUp = bufo;
                stuneado = true;
            break;
            case TIPOBUFF.aumentarDaño:
                aumentoDaño += bufo.cantidad;
            break;
            case TIPOBUFF.reduccionDaño:
                aumentoDaño -= bufo.cantidad;
            break;
        }
        buffos.Add(bufo);
    }

    public void AplicarBuffo (List<Buff> bufos) {
        for (int i = 0; i < bufos.Count; i++) {
            AplicarBuffo(bufos[i]);
        }
    }

    public void EliminarBuffo (Buff bufo) {
        if (buffos.Contains (bufo)) {
            switch(bufo.tipo) {
                case TIPOBUFF.DañoSegundo:
                case TIPOBUFF.CuraSegundo:
                    //NADA
                break;
                case TIPOBUFF.Acelerar:
                buffVelMov -= realVelMov * bufo.cantidad;
                break;
                case TIPOBUFF.KnockUp:
                case TIPOBUFF.Paralizar:
                    stuneado = false;
                    for (int i = 0; i < buffos.Count; i++) {
                        if (bufo!=buffos[i]&&(buffos[i].tipo==TIPOBUFF.Paralizar||buffos[i].tipo == TIPOBUFF.KnockUp)) {
                            stuneado = true;
                        }
                    }
                break;
                case TIPOBUFF.aumentarDaño:
                aumentoDaño -= bufo.cantidad;
                break;
                case TIPOBUFF.reduccionDaño:
                aumentoDaño += bufo.cantidad;
                break;
            }
            buffos.Remove(bufo);
        } else {
            Debug.LogWarning(name + " no contiene este buff");
        }
    }

    public void EliminarBuffo (int bufo) {
        if (bufo<0 || bufo>=buffos.Count) {
            Debug.LogWarning("Bufo no encontrardo");
            return;
        }
        EliminarBuffo(buffos[bufo]);
    }

    public void EliminarBuffo (string nombreBufo) {
        for (int i = 0; i < buffos.Count; i++) {
            if (buffos[i].nombreID == nombreBufo) {
                EliminarBuffo(buffos[i]);
                return;
            }
        }
    }

    public bool ContieneBuffo (string nombreBufo) {
        for(int i = 0; i < buffos.Count; i++) {
            if(buffos[i].nombreID == nombreBufo) {
                return true;
            }
        }
        return false;
    }

    public Buff GetBuff (string nombreBufo) {
        for(int i = 0; i < buffos.Count; i++) {
            if(buffos[i].nombreID == nombreBufo) {
                return buffos[i];
            }
        }
        return null;
    }

    public void AñadirCantidadBuffo(string nombreBufo, float cantidad, bool renovar = false) {
        for(int i = 0; i < buffos.Count; i++) {
            if(buffos[i].nombreID == nombreBufo) {
                Buff _bufo = buffos[i];
                EliminarBuffo(_bufo);
                _bufo.AumentarCantidad(cantidad);
                AplicarBuffo(_bufo);
                if(renovar)
                    buffos[i].RenovarEfecto();
                return;
            }
        }
    }
}
