﻿using UnityEngine;
using System.Collections.Generic;

public enum BANDO { Aliado = 0, Enemigo = 1}
public enum TIPO { Personaje = 0, Estructura = 1, Objeto = 2}

public class ObjectLoL : MonoBehaviour {

    public GameManager manager;

    public int ID;
    public int nivel;

    public float vidaActual, vidaMaxima;
    public float dañoAtaque, velAtaque;
    public float rangoAtaque, velMovimiento;

    public float tiempoDisparo = 0.1f;

    //Variables opciones modificables desde el script hijo
    public bool destruirEntidad = true;
    public bool soloTorres = false;

    public BANDO bando = BANDO.Aliado;
    public TIPO tipo = TIPO.Personaje;

    public IObjectLoL interfazObjeto;
    
    public GameObject misil;
    public GameObject origenDisparo;

    [Space(10)]
    public AudioClip sonidoDisparo;

    [HideInInspector]
    public bool configurado = false;

    public ObjectLoL objetivo;
    
    
    public const float deltaMove = 0.2f;
    public const float vision = 1.5f;
    public const float difRango = 400f;
    public float lastAttack = 0;

    [HideInInspector] public Animator animator;
    [HideInInspector] public Rigidbody rb;
    [HideInInspector] public Collider coll;

    void Awake() {
        animator = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
        coll = GetComponent<Collider>();
    }

    public void InflingirDaño (DañoRecibido daño) {
        interfazObjeto.RecibirDaño(daño);
    }

    public void InflingirDaño (float daño) {
        InflingirDaño(new DañoRecibido(daño));
    }

    public void Destruir () {
        if(destruirEntidad)
            Destroy(this.gameObject);
    }

    public void EncontrarObjetivo() {
        ObjectLoL lastObject = null;
        
        if (!soloTorres) {
            for(int i = 0; i < manager.listaPersonajes.Count; i++) {
                ObjectLoL personaje = manager.listaPersonajes[i];

                if(personaje != null && personaje.bando != bando && vision > Vector3.Distance(transform.position, personaje.transform.position)) {
                    if(lastObject == null || Vector3.Distance(lastObject.transform.position, transform.position) > Vector3.Distance(personaje.transform.position, transform.position)) {
                        lastObject = personaje;
                    }
                }
            }
        }

        if (lastObject == null) {
            for(int i = 0; i < manager.listaTorres.Count; i++) {
                ObjectLoL torre = manager.listaTorres[i];

                if(torre != null && torre.bando != bando && vision > Vector3.Distance(transform.position, torre.transform.position)) {
                    if(lastObject == null || Vector3.Distance(lastObject.transform.position, transform.position) > Vector3.Distance(torre.transform.position, transform.position)) {
                        lastObject = torre;
                    }
                }
            }
        }

        objetivo = lastObject;
    }

    public Personaje[] EncontrarPersonajes (BANDO _bando, float rango) {
        return manager.GetPersonaje (transform.position, _bando, rango);
    }

    public void Atacar() {
        if(this.gameObject == null || objetivo == null)
            return;

        DañoRecibido damage = new DañoRecibido(dañoAtaque);

        manager.soundManager.Play(sonidoDisparo);
        
        if(misil != null && objetivo!=null) {
            GameObject _misil = (GameObject)Instantiate(misil, (origenDisparo!= null) ? origenDisparo.transform.position : transform.position, Quaternion.identity);
            Misil _misilScript = _misil.GetComponent<Misil>();

            _misilScript.manager = manager;
            _misilScript.bando = bando;
            _misilScript.layerPersonaje = manager.layerPersonaje;

            _misilScript.origenObj = this;
            _misilScript.objetivoObj = objetivo;
            _misilScript.objetivo = objetivo.transform.position;
            _misilScript.daño = damage;
        } else {
            interfazObjeto.PreAtaque(ref damage);

            objetivo.InflingirDaño(damage);
        }
    }

    public void Atacar (float tiempoEspera) {
        Invoke("Atacar", tiempoEspera);
    }

    public void ReiniciarAutoattack() {
        lastAttack = 1 / velAtaque;
    }
}
