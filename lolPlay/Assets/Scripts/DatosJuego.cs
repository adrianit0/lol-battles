﻿using UnityEngine;
using System.Collections;

public class DatosJuego : MonoBehaviour {
    
    public EstadoJuego estadoJuego;
    public DatosAGuardar datosJuego;
    public BaseDatos baseDatos;

    public Mazo mazoActual;

    public bool conectado = false;

    const int nivelMaximo = 18;

    void Awake() {
        estadoJuego = new EstadoJuego();
        datosJuego = estadoJuego.extraerDatos();
        mazoActual = datosJuego.mazo;
        DontDestroyOnLoad(this.gameObject);
    }

    public void AñadirCartaColeccion(int ID, int nivel) {
        if (ID<0||ID>=baseDatos.campeones.Length) {
            Debug.LogWarning("Este campeón no está en la base de datos.");
            return;
        }
        datosJuego.unidad[ID].nivel[Mathf.Clamp(nivel, 0, nivelMaximo-1)]++;
    }

    /// <summary>
    /// Obtienes una carta superior fusionando 2 cartas inferior
    /// </summary>
    public void FusionarCarta (int ID, int nivel) {
        if(ID < 0 || ID >= baseDatos.campeones.Length) {
            Debug.LogWarning("Este campeón no está en la base de datos.");
            return;
        }
        if (datosJuego.unidad[ID].nivel[nivel]<2) {
            Debug.LogWarning("No hay suficientes cartas para hacer una fusión.");
            return;
        }
        if (nivel == nivelMaximo) {
            Debug.LogWarning("No se puede fusionar una carta de nivel 18.");
            return;
        }
        datosJuego.unidad[ID].nivel[nivel] -= 2;
        datosJuego.unidad[ID].nivel[nivel + 1]++;
    }

    /// <summary>
    /// Fusiona todas las cartas al nivel más alto posible
    /// </summary>
    /// <param name="ID">ID de la carta para fusionar</param>
    public void FusionarTodas (int ID) {
        if(ID < 0 || ID >= baseDatos.campeones.Length) {
            Debug.LogWarning("Este campeón no está en la base de datos.");
            return;
        }
        
        for (int i = 0; i < nivelMaximo; i++) {
            int cartasFusionadas = 0;
            while (datosJuego.unidad[ID].nivel[i]>=2) {
                FusionarCarta(ID, i);
                cartasFusionadas++;
            }

            if (cartasFusionadas>0) {
                Debug.Log("Se han fusionado " + cartasFusionadas + " de nivel " + i);
            }
        }
    }

    public void GuardarInfo() {
        estadoJuego.Guardar(datosJuego);
    }

    public void BorrarInfo(bool eliminarArchivo = false) {
        if(eliminarArchivo)
            estadoJuego.BorrarDatos();
        datosJuego = new DatosAGuardar();
        estadoJuego.Guardar(datosJuego);
    }
}