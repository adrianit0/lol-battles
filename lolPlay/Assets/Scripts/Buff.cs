﻿using UnityEngine;
using System.Collections;

public enum TIPOBUFF { DañoSegundo, CuraSegundo, Acelerar, Paralizar, Parar, KnockUp, reduccionDaño, aumentarDaño }
public enum TIEMPO { Temporal, Persistente}
public enum ADITIVO { Aditivo, unico, renovar}
public enum CC { Buff, Debuff};

[System.Serializable]
public class Buff {
    public string nombreID;
    public TIPOBUFF tipo;
    public TIEMPO tipoTemporal;
    public ADITIVO aditivo;
    public CC tipoCC;

    public float cantidad;

    public bool cantidadTotal;

    //ESPECIAL PARA KNOCKUP
    public Vector3 direccion;
    public float distancia, altura;

    public float tiempoTotal;
    public float tiempoActual=0;

    /// <summary>
    /// Creación de buff standard
    /// </summary>
    public Buff (string _nombreID, TIPOBUFF _tipo, TIEMPO _temporal, ADITIVO _aditivo, CC _tipoCC, float _cantidad, bool _cantidadTotal, float _tiempo) {
        nombreID = _nombreID;

        tipo = _tipo;
        tipoTemporal = _temporal;
        aditivo = _aditivo;
        tipoCC = _tipoCC;

        cantidad = _cantidad;
        cantidadTotal = _cantidadTotal;
        tiempoTotal = _tiempo;

        direccion = Vector3.zero;
        distancia = altura = 0;

        tiempoActual = 0;
    }

    /// <summary>
    /// Creación de buff standard, pero sin contar el valor de cantidadTotal
    /// </summary>
    public Buff(string _nombreID, TIPOBUFF _tipo, TIEMPO _temporal, ADITIVO _aditivo, CC _tipoCC, float _cantidad, float _tiempo) {
        nombreID = _nombreID;

        tipo = _tipo;
        tipoTemporal = _temporal;
        aditivo = _aditivo;
        tipoCC = _tipoCC;

        cantidad = _cantidad;
        cantidadTotal = false;
        tiempoTotal = _tiempo;

        direccion = Vector3.zero;
        distancia = altura = 0;

        tiempoActual = 0;
    }

    /// <summary>
    /// Creacion de buff unicamente para los Knock Up
    /// </summary>
    public Buff (string _nombreID, Vector3 _direccion, float _distancia, float _altura, float _tiempo) {
        nombreID = _nombreID;

        tipo = TIPOBUFF.KnockUp;
        tipoTemporal = TIEMPO.Temporal;
        aditivo = ADITIVO.Aditivo;
        tipoCC = CC.Debuff;

        cantidad = 0;
        cantidadTotal = false;
        tiempoTotal = _tiempo;

        direccion = _direccion;
        distancia = _distancia;
        altura = _altura;

        tiempoActual = 0;
    }

    public void RenovarEfecto () {
        tiempoActual = 0;
    }

    public void AumentarCantidad (float aumento) {
        cantidad += aumento;
    }
}
