﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SoundManager : MonoBehaviour {

    public List<AudioSource> listaMusica = new List<AudioSource>();

    public const float globalVolume = 0.2f;

    public void Play (AudioClip clip, float volumen) {
        if(clip == null)
            return;

        for (int i = 0; i < listaMusica.Count; i++) {
            if (!listaMusica[i].isPlaying) {
                listaMusica[i].volume = volumen;
                listaMusica[i].clip = clip;
                listaMusica[i].Play();
                return;
            }
        }
        
        AudioSource source = gameObject.AddComponent<AudioSource>();
        source.volume = volumen;
        source.loop = false;
        source.playOnAwake = false;
        source.clip = clip;
        source.Play();
        listaMusica.Add(source);
    }

    public void Play (AudioClip clip) {
        Play(clip, globalVolume);
    }
}