﻿using UnityEngine;
using System.Collections;

public class Objeto : ObjectLoL, IObjectLoL {
    
    public bool temporal = true;
    public float tiempoActual = 50, tiempoTotal = 50;

    public Contenido contenido;

    float tiempoInicialización = 1f;

    public void Configurar () {
        if(configurado) {
            Debug.Log("El objeto ya ha sido configurado con anterioridad.");
            return;
        }
        configurado = true;

        contenido.fondoVida.gameObject.SetActive(false);
        
        interfazObjeto = (IObjectLoL)this;

        coll.enabled = false;
        Invoke("ActivarItem", tiempoInicialización);
    }

    void ActivarItem () {
        contenido.fondoVida.gameObject.SetActive(temporal);
        coll.enabled = true;

        tiempoActual = tiempoTotal;

        InvokeRepeating("Control", deltaMove, deltaMove);
    }

    public void OnDieObject() {
        
    }

    public void PreAtaque (ref DañoRecibido daño) {

    }

    public void RecibirDaño (DañoRecibido daño) {

    }

    void Control () {
        tiempoActual -= deltaMove;

        if (contenido != null) {
            contenido.CambiarVida(tiempoActual, tiempoTotal);
        }

        if (tiempoActual < 0) {
            Destruir();
        }
    }
}
