﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class SpriteAnim {
    public float velocidad = 0.15f;
    public SpriteMov[] movimiento;

    public SpriteAnim () {
        movimiento = new SpriteMov[5];
    }
}

[System.Serializable]
public class SpriteMov {
    public Sprite[] animacion;

    public SpriteMov () {
        animacion = new Sprite[4];
    }
}
