﻿using UnityEngine;
using System.Collections;

public class MovimientoRio : MonoBehaviour {

    public float velocidad = 0.03f;
    Renderer render;

    void Awake() {
        render = GetComponent<Renderer>();
        velocidad = velocidad * Time.fixedDeltaTime;
    }

    void FixedUpdate() {
        render.material.mainTextureOffset = new Vector2(Time.time * velocidad, 0);
    }
}