﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections;

public enum LADOSALIDA { Izquierda, Derecha}

public class GameManager : MonoBehaviour {

    public DatosJuego datosJuego;
    public BaseDatos baseDatos;

    public SoundManager soundManager;

    int nivelMedio = 1;

    public GameObject contenidoPersonaje;
    public GameObject contenidoEstructura;
    public GameObject contenidoObjeto;

    System.DateTime tiempo;
    public Text textoTiempo;

    int frameRate = 0;
    public Text frameRateText;

    public EstadoBando[] bandos = new EstadoBando[2];

    public GameObject[] recorridoObject;
    public Vector3[] recorrido;

    public LayerMask layerPersonaje;
    public AnimationCurve curva;

    public bool desactivarEditor;

    public List<Personaje> listaPersonajes = new List<Personaje>();
    public List<Estructura> listaTorres = new List<Estructura>();

    const bool mostrarRango = false;
    const bool mostrarFrameRate = true;
    
    const float deltaReg = 0.2f;

    void Awake() {
        datosJuego = FindObjectOfType<DatosJuego>();
        if(datosJuego == null) {
            GameObject _obj = new GameObject();
            datosJuego = _obj.AddComponent<DatosJuego>();
            _obj.name = "Datos del juego";
        }

        if (soundManager==null) {
            GameObject _manager = new GameObject();
            SoundManager _sound = _manager.AddComponent<SoundManager>();
            soundManager = _sound;
            _manager.name = "Sound Manager";
        }

        //PRUEBAS: TODO TEEMOS Y VAYNES
        int champ = 0;
        if (champ>=0)
            bandos[0].mazoNivel = new int[6, 2] { { champ, 1 }, { champ, 1 }, { champ, 1 }, { champ, 1 }, { champ, 1 }, { champ, 1 } };
        else
            bandos[0].mazoNivel = new int[6, 2] { { 0, 1 }, { 1, 1 }, { 2, 1 }, { 3, 1 }, { 4, 1 }, { 4, 1 } };
        
    }
    
    void Start () {
        ConfigurarJuego();
        ConfigurarTorres();
        InvokeRepeating("UpdateReg", deltaReg, deltaReg);
        InvokeRepeating("UpdateTime", 1, 1);

        //==========PRUEBAS
        InvokeRepeating("Pruebas", 0.5f, 400);
        
    }

    void Pruebas () {
        CrearUnidad(3, 1, BANDO.Enemigo, LADOSALIDA.Derecha);
    }

    void ConfigurarJuego() {
        if(bandos == null)
            return;

        nivelMedio = 0;
        int cantidad = 0;
        for(int b = 0; b < bandos.Length; b++) {
            if(bandos[b].mazoNivel.GetLength(0) > 0) {
                for(int i = 0; i < bandos[b].mazoNivel.GetLength(0); i++) {
                    bandos[b].listaCartas.Add(bandos[b].mazoNivel[i, 0]);
                    nivelMedio += bandos[b].mazoNivel[i, 1];
                    cantidad++;
                }
            }
            Barajar(b);
        }

        nivelMedio = Mathf.RoundToInt((float)nivelMedio / (float)cantidad);

        for (int boton = 0; boton < bandos[0].botones.Length; boton++) {
            int nuevaID = bandos[0].listaCartas[boton];
            int nuevoBoton = boton;
            bandos[0].botones[boton].botonesCampeones.onClick.RemoveAllListeners();
            bandos[0].botones[boton].botonesCampeones.onClick.AddListener(() => { BotonInvocar(nuevoBoton, nuevaID, LADOSALIDA.Derecha); });
            bandos[0].botones[boton].cantidadCartas.text = baseDatos.campeones[nuevaID].costeMana.ToString();
            bandos[0].botones[boton].imagenesCampeones.sprite = bandos[0].botones[boton].imagenesSombraCampeones.sprite = baseDatos.campeones[nuevaID].spriteCampeon;
            bandos[0].botones[boton].IDCampeon = nuevaID;

            bandos[0].botones[boton].imagenesSombraCampeones.fillAmount = 0;
            bandos[0].botones[boton].botonesCampeones.interactable = true;

            bandos[0].listaCartasJugables[boton] = bandos[0].listaCartas[boton];
            bandos[0].listaCartas.Remove(bandos[0].listaCartasJugables[boton]);
        }

        bandos[0].proximoCampeon.sprite = baseDatos.campeones[bandos[0].listaCartas[0]].spriteCampeon;
    }

    void ConfigurarTorres() {
        for(int i = 0; i < listaTorres.Count; i++) {
            Estructura _script = listaTorres[i];

            _script.manager = this;
            _script.tipo = TIPO.Estructura;

            _script.nivel = 0;
            _script.ID = -1;

            GameObject _contenido = Instantiate(contenidoEstructura);
            Contenido _scriptContenido = _contenido.GetComponent<Contenido>();
            _script.contenido = _scriptContenido;

            _contenido.transform.parent = _script.transform;
            _contenido.transform.localPosition = Vector3.zero;
            _contenido.transform.localRotation = Quaternion.identity;
            _scriptContenido.rangoPersonaje.SetActive(mostrarRango);

            _script.Configurar();
        }
    }

    void Update () {
        if (mostrarFrameRate) {
            frameRate++;
        }
    }
    
    void UpdateReg () {
        for (int b = 0; b < bandos.Length; b++) {
            ModificarMana(b, bandos[b].regeneracionMana * deltaReg);
        }
    }

    void UpdateTime () {
        tiempo = tiempo.AddSeconds(1);
        if (tiempo.Second.ToString().Length == 1)
            textoTiempo.text = tiempo.Minute+":0"+tiempo.Second;
        else
            textoTiempo.text = tiempo.Minute + ":" + tiempo.Second;

        if(mostrarFrameRate) {
            frameRateText.text = frameRate.ToString();
            frameRate = 0;
        }
    }

    public void ModificarMana (int bando, float cantidad) {
        bandos[bando].manaActual = Mathf.Clamp(bandos[bando].manaActual+cantidad, 0, bandos[bando].manaTotal);
        if (bando==0) {
            bandos[bando].textoMana.text = bandos[bando].manaActual + "/" + bandos[bando].manaTotal;
            bandos[bando].barraMana.fillAmount = bandos[bando].manaActual / bandos[bando].manaTotal;

            for (int i = 0; i < bandos[bando].botones.Length; i++) {
                bool activar = bandos[bando].manaActual > baseDatos.campeones[bandos[bando].botones[i].IDCampeon].costeMana;

                bandos[bando].botones[i].botonesCampeones.interactable = activar;
                bandos[bando].botones[i].cantidadCartas.color = (activar) ? Color.white : Color.red;
            }
        }
    }

    void BotonInvocar (int botonID, int ID, LADOSALIDA lado) {
        CambiarBoton(botonID);
        InvocarUnidad(ID, botonID, BANDO.Aliado, lado);
    }

    public void InvocarUnidad  (int ID, int botonID, BANDO bando, LADOSALIDA lado) {
        if (baseDatos.campeones[ID].costeMana>bandos[(int) bando].manaActual) {
            Debug.Log("No tienes mana para invocar al campeón");
            return;
        }

        ModificarMana((int)bando, -baseDatos.campeones[ID].costeMana);
        int nivel = 1;
        for (int i = 0; i < bandos[(int) bando].mazoNivel.GetLength(0); i++) {
            if (ID == bandos[(int)bando].mazoNivel[i, 0]) {
                nivel = bandos[(int)bando].mazoNivel[i, 1];
                break;
            }
        }
        CrearUnidad(ID, nivel, bando, lado);

        bandos[(int)bando].listaCartas.Add(bandos[(int)bando].listaCartasJugables[botonID]);
        bandos[(int)bando].listaCartasJugables[botonID] = bandos[(int)bando].listaCartas[0];
        bandos[(int)bando].listaCartas.RemoveAt(0);
    }

    public void CambiarBoton (int boton) {
        boton = Mathf.Clamp(boton, 0, 2);
        bandos[0].botones[boton].botonesCampeones.onClick.RemoveAllListeners();
        int nuevaID = bandos[0].listaCartas[0];
        bandos[0].botones[boton].botonesCampeones.onClick.AddListener(() => { BotonInvocar(boton, nuevaID, LADOSALIDA.Derecha); });
        bandos[0].botones[boton].cantidadCartas.text = baseDatos.campeones[nuevaID].costeMana.ToString();
        bandos[0].botones[boton].imagenesCampeones.sprite = bandos[0].botones[boton].imagenesSombraCampeones.sprite = baseDatos.campeones[nuevaID].spriteCampeon;
        bandos[0].botones[boton].IDCampeon = nuevaID;

        bandos[0].proximoCampeon.sprite = baseDatos.campeones[bandos[0].listaCartas[1]].spriteCampeon;
    }

    public void CrearUnidad (int unidad, int nivel, BANDO bando, LADOSALIDA lado) {
        if (baseDatos == null) { 
            Debug.LogError("No hay base de datos ligado.");
            return;
        }

        unidad = Mathf.Clamp(unidad, 0, baseDatos.campeones.Length-1);

        if (baseDatos.campeones[unidad].prefabCampeon==null) {
            Debug.LogError("Este campeón no tiene prefab, no puede crearse");
            return;
        }
        GameObject personaje = Instantiate(baseDatos.campeones[unidad].prefabCampeon);
        personaje.transform.position = (bando == BANDO.Aliado) ? recorrido[0] : new Vector3 (recorrido[0].x, recorrido[0].y, -recorrido[0].z);

        Personaje _script = personaje.GetComponent<Personaje>();
        if (_script==null) {
            Destroy(personaje);
            Debug.LogError("Este prefab no tiene el script personaje");
            return;
        }

        _script.manager = this;
        _script.tipo = TIPO.Personaje;
        _script.bando = bando;
        _script.lado = lado;

        _script.nivel = nivel;
        _script.ID = unidad;

        _script.vidaActual = _script.vidaMaxima = baseDatos.campeones[unidad].vida;
        _script.dañoAtaque = baseDatos.campeones[unidad].dañoAtaque;
        _script.velAtaque = baseDatos.campeones[unidad].velAtaque;
        _script.rangoAtaque = baseDatos.campeones[unidad].rangoAtaque;
        _script.velMovimiento = baseDatos.campeones[unidad].velMovimiento;

        listaPersonajes.Add(_script);
        
        GameObject _contenido = Instantiate(contenidoPersonaje);
        Contenido _scriptContenido = _contenido.GetComponent<Contenido>();
        _script.contenido = _scriptContenido;

        _contenido.transform.parent = personaje.transform;
        _contenido.transform.localPosition = Vector3.zero;
        _contenido.transform.localRotation = Quaternion.identity;
        _scriptContenido.rangoPersonaje.SetActive(mostrarRango);
        
        _script.Configurar();

        personaje.SetActive(true);
    }

    /// <summary>
    /// Metodo genérico para crear objetos
    /// </summary>
    public void CrearObjeto (Objeto objeto, BANDO bando) {
        objeto.manager = this;
        objeto.tipo = TIPO.Objeto;
        objeto.bando = bando;

        //Configuracion del contenido
        GameObject _contenido = Instantiate(contenidoObjeto);
        Contenido _scriptContenido = _contenido.GetComponent<Contenido>();
        objeto.contenido = _scriptContenido;

        _contenido.transform.parent = objeto.transform;
        _contenido.transform.localPosition = Vector3.zero;
        _contenido.transform.localRotation = Quaternion.identity;
        _scriptContenido.rangoPersonaje.SetActive(mostrarRango);

        objeto.Configurar();
    }

    public void Barajar (int bando) {
        if(bandos == null || bandos.Length == 0)
            return;

        List<int> nuevaLista = bandos[bando].listaCartas;
        bandos[bando].listaCartas = new List<int>();

        for (;nuevaLista.Count > 0;) {
            int rand = Random.Range(0, nuevaLista.Count);
            bandos[bando].listaCartas.Add(nuevaLista[rand]);
            nuevaLista.RemoveAt(rand);
        }
    }

    public Vector3 siguienteEntidad (Vector3 actual, BANDO bando, LADOSALIDA lado) {
        Vector3 pos = new Vector3(actual.x, actual.y, (bando == BANDO.Aliado) ? actual.z : -actual.z);
        
        for (int i = 0; i < recorrido.Length; i++){
            if (pos.z <= recorrido[i].z) {
                if (i==(recorrido.Length-1))
                    return GetVector(recorrido[i], bando, lado);
                return GetVector(recorrido[i + 1], bando, lado);
            }
        }

        return Vector3.zero;
    }

    Vector3 GetVector (Vector3 vector, BANDO bando, LADOSALIDA lado) {
        return new Vector3((lado == LADOSALIDA.Derecha) ? vector.x : -vector.x, 0, (bando == BANDO.Aliado) ? vector.z : -vector.z);
    }

    public float GetHability (int campeon, int nivel, int habilidad) {
        return baseDatos.campeones[campeon].habilidades[habilidad].valores[Mathf.Clamp(nivel-1, 0, 17)];
    }

    public Personaje[] GetPersonaje (Vector3 origen, BANDO _bando, float rango) {
        List<Personaje> _personajeList = new List<Personaje>();

        for (int i = 0; i < listaPersonajes.Count; i++) {
            if (listaPersonajes[i] != null && rango>Vector3.Distance(origen, listaPersonajes[i].transform.position) && listaPersonajes[i].bando == _bando) {
                _personajeList.Add(listaPersonajes[i]);
            }
        }

        Personaje[] _lista = new Personaje[_personajeList.Count];
        for(int i = 0; i < _personajeList.Count; i++) {
            _lista[i] = _personajeList[i];
        }

        return _lista;
    }
}

[System.Serializable]
public class EstadoBando {
    public int[,] mazoNivel = new int[6, 2];
    public int[] listaCartasJugables = new int[3];
    public List<int> listaCartas = new List<int>();
    public float manaActual = 0, manaTotal = 100;
    public float regeneracionMana = 5f;    //Cada segundo
    public float regeneracionNexo = 0f;     //Cada segundo

    [Space(10)]
    public Text textoVidaNexo;
    public Text textoNombre;

    [Space(10)]
    public Text textoMana;
    public Image barraMana;

    public BotonesCampeones[] botones = new BotonesCampeones[3];
    public Image proximoCampeon;
}

[System.Serializable]
public class BotonesCampeones {
    public int IDCampeon;
    public Button botonesCampeones;
    public Image imagenesCampeones;
    public Image imagenesSombraCampeones;
    public Text cantidadCartas;
}