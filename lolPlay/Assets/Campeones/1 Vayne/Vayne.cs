﻿using UnityEngine;
using System.Collections.Generic;

public class Vayne : Personaje, IPersonaje {

    public GameObject prefabAtaque;
    float tamDisp1 = 0.5f, tamDisp2 = 0.55f, altura = 0.05f;

    List<GameObject> listaDisparos = new List<GameObject>();

    float pirueta, hojaPlata, hojaArruinada;

    bool piruetaHecha = false, dañoPirueta = false;

    ObjectLoL objetivoPasiva;
    int golpesPasiva = 0;

    void Start() {
        OnBorn();
    }

    public void OnBorn() {
        interfazPersonaje = (IPersonaje)this;

        vidaMaxima = vidaActual = manager.GetHability(ID, nivel, 0);
        dañoAtaque = manager.GetHability(ID, nivel, 1);

        pirueta = manager.GetHability(ID, nivel, 2);
        hojaPlata = manager.GetHability(ID, nivel, 3);
        hojaArruinada = manager.GetHability(ID, nivel, 4);
    }
    
    public void OnAttack(ref DañoRecibido damage) {
        if(objetivo == null)
            return;
        if(objetivo.tipo != TIPO.Personaje) {
            golpesPasiva = 0;
            ActualizarBalas();
            return;
        }

        if (objetivo==objetivoPasiva) {
            golpesPasiva++;
            if (golpesPasiva>=3) {
                damage.dañoAtaque += objetivo.vidaMaxima * hojaPlata;
                golpesPasiva = 0;
            }
        } else {
            golpesPasiva = 1;
            objetivoPasiva = objetivo;
        }
        ActualizarBalas();

        float dañoHojaArruinada = Mathf.Round (objetivo.vidaActual * hojaArruinada);
        damage.dañoAtaque += dañoHojaArruinada;
        CurarVida(dañoHojaArruinada/2);

        if (piruetaHecha && !dañoPirueta) {
            dañoPirueta = true;
            damage.dañoAtaque += pirueta;
        }
    }

    public void OnUpdate() {
        if (!piruetaHecha) {
            //Hacer pirueta de ser necesario
        }
    }

    public void OnMove() { }

    public void OnDie() {
        DestruirBalas();
    }

    public void OnDamageReceive(DañoRecibido damage) { }

    void HacerPirueta () {
        if(piruetaHecha)
            return;

        piruetaHecha = true;
    }

    void ActualizarBalas () {
        if(golpesPasiva == listaDisparos.Count) {
            return;
        } else if (golpesPasiva == 1 && objetivo == objetivoPasiva) {
            AñadirBala(tamDisp1);
        } else if (golpesPasiva == 1 && objetivo != objetivoPasiva) {
            DestruirBalas();
            AñadirBala(tamDisp1);
        } else if (golpesPasiva == 2) {
            AñadirBala(tamDisp2);
        } else if (golpesPasiva == 0) {
            DestruirBalas();
        }
    }

    void DestruirBalas () {
        for(int i = 0; i < listaDisparos.Count; i++) {
            Destroy(listaDisparos[i].gameObject);
        }
        listaDisparos = new List<GameObject>();
    }

    void AñadirBala (float tam) {
        if(objetivoPasiva == null)
            return;
        GameObject disparo = (GameObject)Instantiate(prefabAtaque, objetivoPasiva.transform.position, prefabAtaque.transform.rotation);
        disparo.transform.position = new Vector3(disparo.transform.position.x, altura, disparo.transform.position.z);
        disparo.transform.localScale = new Vector3(tam, tam, tam);
        disparo.transform.parent = objetivo.transform;
        listaDisparos.Add(disparo);
    }
}
