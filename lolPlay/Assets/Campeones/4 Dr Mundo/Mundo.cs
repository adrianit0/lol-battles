﻿using UnityEngine;
using System.Collections;

public class Mundo : Personaje, IPersonaje {

    public GameObject lentaAgoniaPrefab;
    GameObject lentaAgoniaObject;

    float curaPasiva, lentaAgonia, masoquismo;

    float rangoPasiva = 200, rangoPasivaReal;

    void Start() {
        OnBorn();
    }

    public void OnBorn() {
        interfazPersonaje = (IPersonaje)this;

        vidaMaxima = vidaActual = manager.GetHability(ID, nivel, 0);
        dañoAtaque = manager.GetHability(ID, nivel, 1);

        curaPasiva = manager.GetHability(ID, nivel, 2);
        lentaAgonia = manager.GetHability(ID, nivel, 3);
        masoquismo = manager.GetHability(ID, nivel, 4);

        rangoPasivaReal = rangoPasiva / difRango;
        soloTorres = true;

        lentaAgoniaObject = Instantiate(lentaAgoniaPrefab);
        lentaAgoniaObject.transform.parent = contenido.transform;
        lentaAgoniaObject.transform.localPosition = new Vector3(0, 0.3f, 0);

        AplicarBuffo(new Buff("curaPasiva Mundo", TIPOBUFF.CuraSegundo, TIEMPO.Persistente, ADITIVO.unico, CC.Buff, curaPasiva, 1));
    }

    void Update () {
        lentaAgoniaObject.transform.rotation = Quaternion.Euler(90, lentaAgoniaObject.transform.rotation.eulerAngles.y - (Time.deltaTime*200), 0);
    }

    public void OnUpdate() {
        Personaje[] perso = EncontrarPersonajes((bando == BANDO.Aliado) ? BANDO.Enemigo : BANDO.Aliado, rangoPasivaReal);

        for (int i = 0; i < perso.Length; i++) {
            perso[i].InflingirDaño(lentaAgonia * deltaMove);
        }
    }

    public void OnAttack(ref DañoRecibido damage) {
        damage.dañoAtaque += masoquismo * (2 - (vidaActual / vidaMaxima));
    }

    public void OnMove() { }

    public void OnDie() { }

    public void OnDamageReceive(DañoRecibido damage) { }
}