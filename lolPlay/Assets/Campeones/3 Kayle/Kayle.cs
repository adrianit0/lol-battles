﻿using UnityEngine;
using System.Collections;

public class Kayle : Personaje, IPersonaje {

    public GameObject prefaIntervencion;

    float dañoPasiva, dañoFuria, tiempoIntervencion;

    float rangoFuria = 200, rangoFuriaReal;
    float rangoInter = 500, rangoInterReal;

    string nombrePasiva, nombreIntervencion;

    void Start() {
        OnBorn();
    }

    public void OnBorn() {
        interfazPersonaje = (IPersonaje)this;

        vidaMaxima = vidaActual = manager.GetHability(ID, nivel, 0);
        dañoAtaque = manager.GetHability(ID, nivel, 1);

        dañoPasiva = manager.GetHability(ID, nivel, 2);
        dañoFuria = manager.GetHability(ID, nivel, 3);
        tiempoIntervencion = manager.GetHability(ID, nivel, 4);

        rangoFuriaReal = rangoFuria / difRango;
        rangoInterReal = rangoInter / difRango;

        nombrePasiva = GetHashCode() + "P";
        nombreIntervencion = GetHashCode() + "I";
    }

    public void OnUpdate() { }

    public void OnAttack(ref DañoRecibido damage) {
        if (objetivo.tipo == TIPO.Personaje) {
            Personaje _pers = objetivo.GetComponent<Personaje>();

            if (_pers.ContieneBuffo (nombrePasiva)) {
                if(_pers.GetBuff(nombrePasiva).cantidad < (dañoPasiva * 5))
                    _pers.AñadirCantidadBuffo(nombrePasiva, dañoPasiva, true);
                else
                    _pers.AplicarBuffo(new Buff(nombrePasiva, TIPOBUFF.aumentarDaño, TIEMPO.Temporal, ADITIVO.renovar, CC.Debuff, dañoPasiva*5, 3));
            } else {
                _pers.AplicarBuffo(new Buff(nombrePasiva, TIPOBUFF.aumentarDaño, TIEMPO.Temporal, ADITIVO.renovar, CC.Debuff, dañoPasiva, 3));
            }
        }

        Personaje[] pers = objetivo.EncontrarPersonajes(objetivo.bando, rangoFuriaReal);
        for(int i = 0; i < pers.Length; i++) {
            pers[i].InflingirDaño(dañoFuria);
        }
    }

    public void OnMove() { }

    public void OnDie() {
        Personaje[] pers = EncontrarPersonajes(bando, rangoInterReal);
        for(int i = 0; i < pers.Length; i++) {
            pers[i].AplicarBuffo(new Buff(nombreIntervencion, TIPOBUFF.reduccionDaño, TIEMPO.Temporal, ADITIVO.renovar, CC.Buff, 10, tiempoIntervencion));
            CrearInter(pers[i], tiempoIntervencion);
        }
    }

    public void OnDamageReceive(DañoRecibido damage) { }

    void CrearInter (Personaje origen, float tiempo) {
        if(origen.contenido == null)
            return;

        GameObject _inter = (GameObject)Instantiate(prefaIntervencion, Vector3.zero, prefaIntervencion.transform.rotation);
        _inter.transform.parent = origen.contenido.transform;
        _inter.transform.localPosition = new Vector3(0, 0.3f, 0);

        Destroy(_inter, tiempo);
    }
}