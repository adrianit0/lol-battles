﻿using UnityEngine;
using System.Collections;

public class Volibear : Personaje, IPersonaje {

    public AudioClip sonidoAtaque, sonidoSpell, sonidoSpellAtaque;

    float curaPasiva, dañoCorrer, dañoUlti;

    float tiempoCorrer, tiempoNecesarioCorrer = 3f, bonusCorrer = 0.5f;
    bool corriendo = false;
    string nombreCorrer = "CorrerVolibear";

    float duracionPasiva = 3f;
    bool pasivaUsada = false;

    void Start() {
        OnBorn();
    }

    public void OnBorn() {
        interfazPersonaje = (IPersonaje)this;

        vidaMaxima = vidaActual = manager.GetHability(ID, nivel, 0);
        dañoAtaque = manager.GetHability(ID, nivel, 1);
        curaPasiva = manager.GetHability(ID, nivel, 2);
        dañoCorrer = manager.GetHability(ID, nivel, 4);
        dañoUlti = manager.GetHability(ID, nivel, 4);
    }

    public void OnUpdate() {
        if (enMovimiento&&!corriendo) {
            tiempoCorrer += deltaMove;
            if (tiempoCorrer>tiempoNecesarioCorrer) {
                animator.SetTrigger("Spell");
                corriendo = true;
                AplicarBuffo(new Buff(nombreCorrer, TIPOBUFF.Acelerar, TIEMPO.Persistente, ADITIVO.unico, CC.Buff, bonusCorrer, 1));
            }
        }
    }

    public void OnAttack(ref DañoRecibido damage) {
        if (corriendo) {
            damage.dañoAtaque += dañoCorrer;
            damage.bufos.Add(new Buff("KnockUp Volibear", objetivo.transform.position - transform.position, 1, 0.6f, 0.5f));
            corriendo = false;
            EliminarBuffo(nombreCorrer);
            //Aplicar CC
        }
        tiempoCorrer = 0;

        //Aplicar daño de la ultimate
    }

    public void OnMove() { }

    public void OnDie() { }

    public void OnDamageReceive(DañoRecibido damage) {
        if(!pasivaUsada && vidaMaxima * 0.3f > vidaActual) {
            pasivaUsada = true;

            AplicarBuffo(new Buff("Elegido tormenta", TIPOBUFF.CuraSegundo, TIEMPO.Temporal, ADITIVO.unico, CC.Buff, curaPasiva, true, duracionPasiva));
        }
    }
}
