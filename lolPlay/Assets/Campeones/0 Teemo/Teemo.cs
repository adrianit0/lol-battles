﻿using UnityEngine;
using System.Collections;

public class Teemo : Personaje, IPersonaje {

    public Objeto seta;
    public AudioClip sonidoPonerSeta;

    float veneno, venenoSeta;
    float duracionVeneno = 4f;

    float tiempoSeta, tiempoSetaTotal = 5, tiempoDuracionSeta = 10;

    bool bufado = true;
    float tiempoBuff = 0, tiempoLastSeta = 0, tiempoTotal = 3;

    string buffoVeneno, buffoVelocidad;

    void Start () {
        OnBorn();
    }

    public void OnBorn() {
        interfazPersonaje = (IPersonaje)this;

        vidaMaxima = vidaActual = manager.GetHability(ID, nivel, 0);
        dañoAtaque = manager.GetHability(ID, nivel, 1);
        veneno = manager.GetHability(ID, nivel, 2);
        venenoSeta = manager.GetHability(ID, nivel, 4);

        buffoVeneno = GetInstanceID().ToString() + "P";
        buffoVelocidad = GetInstanceID().ToString() + "V";

        AplicarBuffo(new Buff(buffoVelocidad, TIPOBUFF.Acelerar, TIEMPO.Persistente, ADITIVO.unico, CC.Buff, manager.GetHability(ID, nivel, 3), 1));
    }

    public void OnUpdate () {
        if (!bufado) {
            tiempoBuff += deltaMove;
            if (tiempoBuff >= tiempoTotal) {
                AplicarBuffo(new Buff(buffoVelocidad, TIPOBUFF.Acelerar, TIEMPO.Persistente, ADITIVO.unico, CC.Buff, manager.GetHability(ID, nivel, 3), 1));
                bufado = true;
            }
        }

        tiempoSeta += deltaMove;
        tiempoLastSeta += deltaMove;
        if (tiempoSeta>=tiempoSetaTotal) {
            PonerSeta();
        }
    }

	public void OnAttack (ref DañoRecibido damage) {
        damage.bufos.Add(new Buff(buffoVeneno, TIPOBUFF.DañoSegundo, TIEMPO.Temporal, ADITIVO.renovar, CC.Debuff, veneno, duracionVeneno));
    }

	public void OnMove () { }
    public void OnDie () {
        if(tiempoLastSeta >= tiempoSetaTotal)
            PonerSeta();
    }

    public void OnDamageReceive (DañoRecibido damage) {
        EliminarBuffo(buffoVelocidad);
        bufado = false;
        tiempoBuff = 0;
        tiempoSeta = 0;
    }

    void PonerSeta () {
        tiempoSeta = 0;
        tiempoLastSeta = 0;

        manager.soundManager.Play(sonidoPonerSeta);

        Objeto estaSeta = (Objeto)Instantiate(seta, transform.position, seta.transform.rotation);
        estaSeta.dañoAtaque = venenoSeta;
        estaSeta.nivel = nivel;
        estaSeta.tiempoActual = estaSeta.tiempoTotal = tiempoDuracionSeta;
        manager.CrearObjeto(estaSeta, bando);
    }
}
