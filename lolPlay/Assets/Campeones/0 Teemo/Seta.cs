﻿using UnityEngine;
using System.Collections;

public class Seta : Objeto, IObjeto {

    public AudioClip sonidoExplosion;

    float rangoSeta = 0;

    float ralentizacion = -0.5f, tiempoEfecto = 4f;
    
    void Start () {
        rangoSeta = 250 / difRango;
        if(contenido != null)
            contenido.SetCanvas(rangoSeta, 1, bando);
    }

	void OnTriggerEnter (Collider coll) {
        Personaje personaje = coll.transform.GetComponent<Personaje>();

        if (personaje != null && personaje.bando != bando) {
            Personaje[] _personajes = EncontrarPersonajes(personaje.bando, rangoSeta);
            for (int i = 0; i < _personajes.Length; i++) {
                _personajes[i].AplicarBuffo(new Buff("SetaDamage", TIPOBUFF.DañoSegundo, TIEMPO.Temporal, ADITIVO.renovar, CC.Debuff, dañoAtaque, tiempoEfecto));
                _personajes[i].AplicarBuffo(new Buff("SetaSlow", TIPOBUFF.Acelerar, TIEMPO.Temporal, ADITIVO.renovar, CC.Debuff, ralentizacion, tiempoEfecto));
            }

            manager.soundManager.Play(sonidoExplosion);
            
            Destruir();
        }
    }
}
