﻿using UnityEngine;
using System.Collections;

public class Garen : Personaje, IPersonaje {

    public GameObject rotacion;

    float dañoJuicio, tiempoPerse;

    float tiempoPasiva = 0;
    string nombrePasiva = "pasiva Garen";
    bool pasivaActivada = false, atacado = false;

    float rangoJuicio = 300, rangoJuicioReal;
    float tiempoParaJuicio, tiempoTotalJuicio = 0.3f;

    void Start() {
        OnBorn();
    }

    public void OnBorn() {
        interfazPersonaje = (IPersonaje)this;

        vidaMaxima = vidaActual = manager.GetHability(ID, nivel, 0);
        dañoAtaque = manager.GetHability(ID, nivel, 3);

        dañoJuicio = manager.GetHability(ID, nivel, 1);
        tiempoPerse = manager.GetHability(ID, nivel, 2);

        rangoJuicioReal = rangoJuicio / difRango;

        ReiniciarAutoattack();
        rotacion.SetActive(false);
        rotacion.transform.parent = contenido.transform;
    }

    void Update () {
        if (rotacion.activeSelf) {
            rotacion.transform.rotation = Quaternion.Euler(90, rotacion.transform.rotation.eulerAngles.y + (Time.deltaTime * 400), 0);
        }
    }

    public void OnUpdate() {
        tiempoPasiva += deltaMove;
        if (!pasivaActivada && tiempoPasiva>tiempoPerse) {
            pasivaActivada = true;
            AplicarBuffo(new Buff(nombrePasiva, TIPOBUFF.CuraSegundo, TIEMPO.Persistente, ADITIVO.unico, CC.Buff, vidaMaxima * 0.33f, 1));
        }
        
        if (atacado) {
            if (tiempoParaJuicio<=tiempoTotalJuicio) {
                tiempoParaJuicio += deltaMove;
                if (tiempoParaJuicio > tiempoTotalJuicio) {
                    rotacion.SetActive(true);
                    animator.SetBool("Spell", true);
                }
                return;
            }
            
            Personaje[] _pers = EncontrarPersonajes((bando == BANDO.Aliado) ? BANDO.Enemigo : BANDO.Aliado, rangoJuicioReal);

            if (_pers.Length==0) {
                atacado = false;
                tiempoParaJuicio = 0;
                ReiniciarAutoattack();
                rotacion.SetActive(false);
                animator.SetBool("Spell", false);
                return;
            }

            for(int i = 0; i < _pers.Length; i++) {
                _pers[i].InflingirDaño(dañoJuicio * deltaMove);
            }
        }
    }

    public void OnAttack(ref DañoRecibido damage) {
        atacado = true;
    }

    public void OnMove() { }

    public void OnDie() { }

    public void OnDamageReceive(DañoRecibido damage) {
        tiempoPasiva = 0;
        if (pasivaActivada) {
            pasivaActivada = false;
            EliminarBuffo(nombrePasiva);
        }
    }
}